(function (Drupal, $) {

    "use strict";

    Drupal.behaviors.geoportailMap = {

        attach: function (context, settings) {

            $(context).find('.geoportail-map').once('.ol-viewport').each(function () {

                var latlon = settings.geoportail_map.geoportail_map_library.locations;
                var bglayer = settings.geoportail_map.geoportail_map_library.bglayer;
                var zoom = settings.geoportail_map.geoportail_map_library.zoom;
                var layers = settings.geoportail_map.geoportail_map_library.layers;
                
                var map = new lux.Map({
                    target: 'map1',
                    bgLayer: bglayer, //cdt_lignes_rgtr, basemap_2015_global ...
                    layers: layers, //optional
                    zoom: zoom,
                    positionSrs: 4326, // optional, default EPSG:2169 the luxembourg coordinate reference system, EPSG:4326 the WGS84 coordinate reference system, default EPSG:2169 the luxembourg coordinate reference system
                    position: [latlon[0][0], latlon[0][1]],
                });
            });
        }
    };
}) (Drupal, jQuery);




