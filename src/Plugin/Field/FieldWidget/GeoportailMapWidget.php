<?php

namespace Drupal\geoportail_map\Plugin\Field\FieldWidget;


use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\Plugin\Field\FieldWidget\GeofieldLatLonWidget;

/**
 * Plugin implementation of the 'geoportail_map' widget.
 *
 * @FieldWidget(
 *     id = "geoportail_map_widget",
 *     label= @Translation("Geoportail Map Widget"),
 *     field_types={
            "geofield"
 *     }
 * )
 */
class GeoportailMapWidget extends GeofieldLatLonWidget implements WidgetInterface
{
    /**
     * Lat Lon widget components.
     *
     * @var array
     */
    public $components = ['lon', 'lat'];
    /**
     * {@inheritdoc}
     */
    public  static function defaultSettings()
    {
        return [
            'bglayer' => 'basemap_2015_global',
            'layers' => 'addresses',
        ] + parent::defaultSettings();
    }
    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $elements['bglayer'] = [
            '#type' => 'textfield',
            '#title' => t('The Id of the background layer'),
            '#default_value' => $this->getSetting('bglayer'),
        ];
        $elements['layers'] = [
            '#type' => 'select',
            '#title' => t('The Id of the overlay layer'),
            '#options' => [
                'none' => t('No overlay'),
                'addresses' => t('Addresses'),
                'cdt_lignes_rgtr' => t('Bus lines RGTR'),
            ],
            '#default_value' => $this->getSetting('layers'),
            '#description' => t('Layers to display on top of the base map.')
        ];
        $elements['zoom'] = [
            '#type' => 'number',
            '#title' => t('The starting zoom level.'),
        ];

        return $elements;
    }
    /**
    * {@inheritdoc}
    */
    public function settingsSummary()
    {
        $summary = [];
        $summary[] = t('Background layer: @bglayer', ['@bglayer' => $this->getSetting('bglayer')]);
        $summary[] = t('Overlay layer: @layers', ['@layers' => $this->getSetting('layers')]);
        $summary[] = t('Starting zoom: @zoom', ['@zoom' => $this->getSetting('zoom')]);

        return $summary;
    }
    /**
     * Implements \Drupal\field\Plugin\Type\Widget\WidgetInterface::formElement().
     *
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {
        $element = [];

        $element['bglayer'] += [
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->bglayer) ? $items[$delta]->bglayer : NULL,
            '#required' => $element['#required'],
        ];
        $element['layers'] += [
            '#type' => 'select',
            '#default_value' => isset($items[$delta]->layers) ? $items[$delta]->layers : NULL,
        ];
        $element['zoom'] += [
            '#type' => 'number',
            '#default_value' => isset($items[$delta]->zoom) ? $items[$delta]->zoom : NULL,
            '#required' => $element['#required'],
        ];
        return $element;

    }
    /**
     * {@inheritdoc}
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state)
    {

        return $values;
    }
}