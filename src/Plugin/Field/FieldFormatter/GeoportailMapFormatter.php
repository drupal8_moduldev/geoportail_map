<?php

namespace Drupal\geoportail_map\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\Markup;
use Drupal\geofield_map\Services\MarkerIconService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Language\Language;

/**
 * Plugin implementation of the 'geoportail_map' formatter.
 *
 * @FieldFormatter(
 *   id = "geoportail_map",
 *   label = @Translation("Geoportail Map"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeoportailMapFormatter extends FormatterBase implements ContainerFactoryPluginInterface
{

    /**
     * Empty Map Options.
     *
     * @var array
     */
    protected $emptyMapOptions = [
        '0' => 'Empty field',
        '1' => 'Custom Message',
        '2' => 'Empty Map Centered at the Default Center',
    ];

    /**
     * The config factory service.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $config;

    /**
     * Entity manager service.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Entity display repository.
     *
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     */
    protected $entityDisplayRepository;


    /**
     * The Link generator Service.
     *
     * @var \Drupal\Core\Utility\LinkGeneratorInterface
     */
    protected $link;

    /**
     * The EntityField Manager service.
     *
     * @var \Drupal\Core\Entity\EntityFieldManagerInterface
     */
    protected $entityFieldManager;

    /**
     * The geoPhpWrapper service.
     *
     * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
     */
    protected $geoPhpWrapper;

    /**
     * The Renderer service property.
     *
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     */
    protected $renderer;

    /**
     * The module handler to invoke the alter hook.
     *
     * @var \Drupal\Core\Extension\ModuleHandlerInterface
     */
    protected $moduleHandler;

    /**
     * The Icon Managed File Service.
     *
     * @var \Drupal\geofield_map\Services\MarkerIconService
     */
    protected $markerIcon;

    /**
     * GeoportailMapFormatter constructor.
     * @param $plugin_id
     * @param $plugin_definition
     * @param FieldDefinitionInterface $field_definition
     * @param array $settings
     * @param $label
     * @param $view_mode
     * @param array $third_party_settings
     * @param ConfigFactoryInterface $config_factory
     * @param TranslationInterface $string_translation
     * @param LinkGeneratorInterface $link_generator
     * @param EntityTypeManagerInterface $entity_type_manager
     * @param EntityDisplayRepositoryInterface $entity_display_repository
     * @param EntityFieldManagerInterface $entity_field_manager
     * @param GeoPHPInterface $geophp_wrapper
     * @param RendererInterface $renderer
     * @param ModuleHandlerInterface $module_handler
     * @param MarkerIconService $marker_icon_service
     */
    public function __construct(
        $plugin_id,
        $plugin_definition,
        FieldDefinitionInterface $field_definition,
        array $settings,
        $label,
        $view_mode,
        array $third_party_settings,
        ConfigFactoryInterface $config_factory,
        LinkGeneratorInterface $link_generator,
        EntityTypeManagerInterface $entity_type_manager,
        EntityDisplayRepositoryInterface $entity_display_repository,
        EntityFieldManagerInterface $entity_field_manager,
        GeoPHPInterface $geophp_wrapper,
        RendererInterface $renderer,
        ModuleHandlerInterface $module_handler
    ){
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
        $this->config = $config_factory;
        $this->link = $link_generator;
        $this->entityTypeManager = $entity_type_manager;
        $this->entityDisplayRepository = $entity_display_repository;
        $this->entityFieldManager = $entity_field_manager;
        $this->geoPhpWrapper = $geophp_wrapper;
        $this->renderer = $renderer;
        $this->moduleHandler = $module_handler;
    }

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('config.factory'),
            $container->get('link_generator'),
            $container->get('entity_type.manager'),
            $container->get('entity_display.repository'),
            $container->get('entity_field.manager'),
            $container->get('geofield.geophp'),
            $container->get('renderer'),
            $container->get('module_handler')
        );
    }
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return [
                'bglayer' => 'basemap_2015_global',
                'layers' => 'addresses',
                'zoom' => 18,
                'langcode' => 'en',
            ] + parent::defaultSettings();
    }
    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = [];
        $elements = parent::settingsForm($form, $form_state);

        $elements['bglayer'] = [
            '#type' => 'textfield',
            '#title' => t('The Id of the background layer'),
            '#default_value' => $this->getSetting('bglayer'),
        ];
        $elements['layers'] = [
            '#type' => 'select',
            '#title' => t('The Id of the overlay layer'),
            '#options' => [
                'none' => t('No overlay'),
                'addresses' => t('Addresses'),
                'cdt_lignes_rgtr' => t('Bus lines RGTR'),
            ],
            '#default_value' => $this->getSetting('layers'),
            '#description' => t('Layers to display on top of the base map.')
        ];
        $elements['zoom'] = [
            '#type' => 'number',
            '#title' => t('The starting zoom level.'),
            '#default_value' => $this->getSetting('zoom'),
        ];

        return $elements;
    }
    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        $summary[] = t('Background layer: @bglayer', ['@bglayer' => $this->getSetting('bglayer')]);
        $summary[] = t('Overlay layer: @layers', ['@layers' => $this->getSetting('layers')]);
        $summary[] = t('Starting zoom: @zoom', ['@zoom' => $this->getSetting('zoom')]);

        return $summary;
    }
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        foreach ($items as $delta => $item) {
            if ($item->isEmpty()) {
                continue;
            }
            $value = $item->getValue();
            if ($value['geo_type'] !== 'Point') {
                continue;
            }

            $locations[] = [$value['lat'],$value['lon']];

            $elements[$delta] = [
              '#theme' => 'geoportail_map',
//              '#bglayer' => $this->viewElements('bglayer'),
//            '#height' => $settings['height'],
//            '#scale' => $settings['scale'],
//            '#locations' => $locations,
//            '#zoom' => $settings['zoom'],
//                '#langcode' => $language,
//            '#static_map_type' => $settings['static_map_type'],
//            '#apikey' => (string) $this->getGmapApiKey(),
//            '#marker_color' => $settings['marker_color'],
//            '#marker_size' => $settings['marker_size'],
                '#attached' => [
                    'library' => [
                        'geoportail_map/geoportail_map_library',
                        'geoportail_map/geoportail_map_v3apiloader',
                    ],
                    'drupalSettings' => [
                        'geoportail_map' => [
                            'geoportail_map_library' => [
                                'locations' => $locations,
                                'bglayer' => $this->getSetting('bglayer'),
                                'zoom' => $this->getSetting('zoom'),
                                'layers' => $this->getSetting('layers')
                            ]
                        ],
                    ],
                ],
            ];
        }
//        var_dump($elements);
//        die();
        return $elements;
    }
}